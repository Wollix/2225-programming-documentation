- [Introduction](#introduction)
  - [How is this guide meant to be used?](#how-is-this-guide-meant-to-be-used)
  - [How am I supposed to do the programming in this book?](#how-am-i-supposed-to-do-the-programming-in-this-book)
  - [What are my expectations as a programmer?](#what-are-my-expectations-as-a-programmer)
  - [Will this guide teach me everything about programming?](#will-this-guide-teach-me-everything-about-programming)
  - [Let’s begin](#lets-begin)
- [Electrical](#electrical)
  - [Rundown of main electrical components](#rundown-of-main-electrical-components)
  - [Best practices](#best-practices)
  - [Conclusion](#conclusion)
- [Programming](#programming)
  - [Basics of Java](#basics-of-java)
  - [Basics of Programming](#basics-of-programming)
    - [Algorithms](#algorithms)
  - [Robotics-Specific Programming](#robotics-specific-programming)
    - [WPILib](#wpilib)
    - [The Scheduler](#the-scheduler)
    - [Commands and Subsystems](#commands-and-subsystems)
- [Conclusion](#conclusion-1)
- [Appendix A - Wiring Schematic](#appendix-a---wiring-schematic)
<!-- There was something here that couldn't be converted.  It may be nothing, but you should compare this spot with the original to make sure nothing is missing.  Either way, delete this comment afterward. -->

# Introduction

Hello, and welcome to the robotics programming/electrical[^1] (NOTE:  Since the programming and electrical side of the robot are so interconnected, you’re expected to have a knowledge of both. I will be referring to the reader as a "programmer" for the rest of the book, but you are also technically an “electrician” working on the robot. Focusing more on one or the other is bound to happen, but it just is easier if everyone is more knowledgeable.
) team! This document contains a lot of the concepts integral to becoming a programmer and/or electrician on our robotics team. It serves a few functions for our team as a whole:

1. We are able to train in new programmers without taking up a significant portion of our senior programmer’s (NOTE:  A senior programmer is not necessarily a programmer who happens to be a senior, they are just one of the programmers on the team that actually knows what they are doing.
) time. They, and eventually, you, have a lot of work to do to get the robot running smoothly, so to take up so much time on training every new programmer would be a momentous and unreasonable task.

2. We are able to document a lot of stranger concepts and findings about the programming and wiring of a robot, so that later generations of robotics members aren’t confused by the same pitfalls that we have to deal with in the present.

3. This documentation, in a more general way, is able to keep the collective programming knowledge of the robotics team alive and growing, so that our team only gets better as the years go on. Before this, when older members left, a lot of knowledge about the programming, electronics, and mechanical sides of the robot was lost, forcing the team to essentially start over every 4 years. If this is kept up to date, and actually used, it can provide a way for this to stop happening, and maybe even pull our team out of our mediocrity rut (NOTE:  When this guide was written, our performances in competitions were not amazing. Okay, but not incredible
). Hopefully, future Sams (NOTE:  Sam was the name of a programming/electrical captain on our team. He was the only one who knew programming/electrical concepts for most of his run as a captain (2015-2019). The Sam is now a more general title to refer to our best programmer and electrician. It’s a title, like Czar (which comes from the name Caesar). Remember, we have only seen further by standing on the shoulders of Sams.) can keep this documentation alive.

But, even with the *why* out of the way, you may still have a few questions, and hopefully I can answer them.

## How is this guide meant to be used?

You should not only be reading along with everything listed here, but should actually be attempting it. If possible. This is true for most technical books related to software. It’s not enough to just read about how something should work, especially in programming. Actually applying the knowledge shows that you have a good grasp of what you’re reading, but also makes it easier to remember in the long run. In fact, most programmers that are self-taught, such as the current senior programmers, usually teach themselves by taking on different coding challenges or projects and then just figuring out how to do it through google and other sources.

Beyond that, this book can also be a good reference for senior programmers who are looking for some basic coding concept they have forgotten about the robot.

## How am I supposed to do the programming in this book?

There is actually an entire guide to setting up the software most people on the team use to code. It can be found [here](https://docs.google.com/document/d/1A1y4yzvrbSrGjqKmnDDzu1Tn7YcH7Oyq-k4rbZsYy3A/edit?usp=sharing) (NOTE:  This was written a lot earlier than this guide, and was originally intended as the documentation for the robotics team as a whole. It’s writing is bad in parts, but it is still a useful guide. It could be fixed, but that would be unnecessary and take a lot of time and effort.
). It is very in-depth and should be able to answer most of your questions. If you need a different software set-up, you should ask the different programmers on the team. 

## What are my expectations as a programmer?

You should be someone actually interested in learning programming, which is not an easy thing to do. A programmer gets to do a lot of fun things, but, as a consequence, also has to learn a lot more than a mechanical (NOTE:  Our team is split into a few different groups. We are the programming/electrical group. The mechanical group refers to anyone who mainly focuses on building the actual robot.) member of the robotics team. You’ll have to spend a lot of time outside of robotics meetings learning about programming. If you’re someone who’s cut out for this kind of thing, you’ll probably want to do that anyway.

It’s very hard to learn programming if you’re not someone who’s motivated enough. Senior programmers could spend hours trying to teach you everything you need to know about programming, but if you’re not applying this knowledge, it's easy to forget. 

## Will this guide teach me everything about programming?

No. Even if I could teach you everything about programming in one piece of writing, I wouldn’t, because that would take a lot of time and effort when there are already many great sources for learning more general programming topics, which are usually written by people who are really good at programming and writing.

Beyond even that, it's impossible to teach someone everything about programming. Even learning everything about one programming language is hard because it is actually like a language. There is a lot of stuff to learn, and most of it is useless.

What’s more important is that you learn the basics of Java, a lot of the structure and methods (NOTE:  You haven’t learned methods yet, but don’t panic. I am just using it to illustrate my example.
) used in most of our robots code, and how to solve coding problems that you don’t have an immediate answer for.

Sometimes, senior programmers are asked if we have "taught everything we know to other people". In terms of mechanical, that is a completely feasible task. But, as you’ll soon learn, that is not something that a programmer can really do. And even if they could, it's not too important to do. Thus, the answer to this question is always “No,” and it makes anyone who is not a programmer terrified (NOTE:  If you ever are a senior programmer, you’ll quickly learn there are a lot of things most people on the team don’t understand about programming. Leading to them asking you to do very stupid things that make very little sense. This is a problem that can even extend to the mentors of the team.).

## Let’s begin

With the general FAQ of this completed, let’s get down into the nitty-gritty of what you need to know.

# Electrical

The electrical side of the robot is not a very difficult thing to understand. Mainly because FRC (NOTE:  FRC stands for FIRST Robotics Competition. It is the name of the robotics program that our team is a part of. People often use FRC to also mean the people who run FRC.
) provides us with most of the main components required to wire together a robot. Beyond that, there is even a wiring guide (NOTE:  As of 2020, this guide is found here.
) to teach people how to wire together the components. From there, it's as simple as modifying the base wiring to fit the needs of the robot. 

Thus, the documentation here is not comprehensive, but does contain a lot of useful pieces of information. It can serve as good context for the wiring guide, a reference to different pieces of electrical knowledge, and has some advice on electrical best practices. Always refer to the most up to date information though. If the mentors suggest doing something in a way that conflicts with the best practices, always refer to them. And try to use the most up to date wiring guide.

## Rundown of main electrical components

This is a quick summary of all the different components usually used on our robot.

* Roborio - this is the main ‘computer’ of our robot. It processes the information we send it from the radio, and uses that information to tell the other components what to do.

* Power Distribution Panel (PDP or PDB) - This device takes in the power from the battery and provides it to the devices wired into it. This panel is controlled by the roborio, so, it does not give power to something until the roborio tells it to. Thus, the PDP is a really good way to provide power to the motors, which you want to be able to turn on and off.

* Voltage Regulator Module (VRM) - This provides different voltages constantly to devices like cameras or sensors.  This is important because it means the roborio doesn’t need to tell the VRM to power something. So, it’s good for powering things like  a camera, which you’re never going to turn off and don’t want to turn off. It provides power at 12V and 5V, with amperages of 500 mA and 2A.

* Radio - it looks like a wi-fi router, and that's exactly what it is. We connect a computer to the radio, and then use the radio to talk with the robot wirelessly. It does need to be flashed (NOTE:  Not as in taking off clothes. Flashing, in this case, means to install software onto the radio) to properly work however.

* Motor controllers - these little black boxes wire into the PDP and then wire into a motor. These motor controllers allow us to control the motors with software, and, if an encoder is attached to the motor controller and mother, we can also get information from the motor.

* Pneumatics Control Module (PCM) - we almost never use this on our robot, because pneumatics can be extremely messy (NOTE:  The only time they are not extremely messy is when you know what you are doing. Unsurprisingly, we almost never know what we are doing.
). But this controls the different pneumatics on the robot, and is required if we ever decide to work with them.

That doesn’t seem like a lot. And that’s because it’s not. But for someone who doesn’t know electronics, and doesn’t have this context, seeing the mess of wires on the robot can look intimidating, and it becomes hard to work with (NOTE:  We also put all of the electronics in a bucket once, which also made the electronics hard to work with.
). Other pieces like sensors may also be wired into the robot, but they are usually better to handle on a case by case basis.

## Best practices

I’m not exactly sure how to organize the best practices, so I’m just going to write them all in different paragraphs (NOTE:  Reorganization could be a job of a future Sam.
).

You should try to stick to one type of connector throughout the robot. By using mainly one connector, it is easy to replace the connectors when they fall off because you don’t need to question what type they are. For choosing a connector to use throughout the robot, it should be something that is:

* Abundant

* Cheap

* Easy to differentiate male and female ends (NOTE:  In electrical, the connectors are either referred to as ‘male’ or ‘female’. ‘Male" ends usually have a metal bit that sticks out, and ‘female’ ends usually are designed to receive that metal bit. If you don’t like this terminology, I’m sorry. I don’t make the rules.
)

* Made of an easy crimp-able material

Generally, this will just be a connector that’s already being used, or whatever connector is the most abundant in the Kit of Parts (NOTE:  If you are out of the loop, the Kit of Parts is a tote of materials we are given at the start of the season.). Currently, as of 2020, we use fully insulated spade connectors.

When connectors are crimped to the end of a wire, a piece of insulation in the form of shrink tube or electrical tape should cover the area where the connector and wire meet. This is to avoid shorting the wires, and shocking people.

To make the positive and negative wires easily differentiable, especially for color-blind people, male ends should always go onto positive wires, and female ends should always go onto negative wires (NOTE:  This choice is arbitrary, but choosing which type of connector goes onto which wire makes things easier to maintain and see. Male and female ends have no functional difference for our purposes, but doing this just makes our lives easier in the long run.).

The stripped metal end of a wire should always touch some metal part of the connector. This is more of advice rather than a practice.

There is almost no reason to ever have a metal piece of the wiring exposed. If you have no reason to leave metal exposed, insulate it somehow to ensure that people don’t shock themselves on the robot. Safety is important, especially in the realm of electronics.

Soldering should not be done randomly. Always have a good reason to solder something. Beyond that, make sure the solder job is good. If you are not confident in your own abilities, ask a mentor.

## Conclusion

The wiring of the robot is not too hard of a task. It’s just a necessary thing to do, and with these few rules and bits of information in mind, it should not be hard to put together the wiring of the robot with the wiring guide. You’ll notice that at no point did I mention how to strip wires or crimp connectors on, and that's because describing it in writing is very in-effective. Consult google or a senior programmer for help with that. 

As for how to wire the hardware components together, a basic example of how to do that can be seen under *Appendix A*. It contains a basic schematic of the robots wiring. Otherwise, again, there is a wiring guide released by FRC.

Future writers or Sams might be interested in updating the best practices as we determine more good things to do with the wiring. Potentially adding in a general wiring schematic could also be a good step.

# Programming

"Finally! The part I’ve been waiting for!" I hear you say (NOTE:  I am, in fact, telepathic and can divine future events.
). And that’s a good point. Hopefully you can realize that the previous 7 pages have been building up your context to be prepared for this section, and now, we can go over more specific programming bits.

## Basics of Java

We program our robot in Java (NOTE:  As of 2020. This is subject to possibly change, but that’s not likely. However, if any future team switches back to labview, I and Sam O’Brien himself, will be very disappointed in you.
). This is not the easiest programming language to understand, but it is a very popular one and a useful one to know if you decide to go into a career of programming.

When I tried to write the documentation for programming a robot the first time, I tried to write out a comprehensive beginner’s guide to Java. I’ve since learned that that is really hard to do, and ultimately pointless, because there are better resources online. Two of which are [W3Schools](https://www.w3schools.com/java/) and [tutorialspoint](https://www.tutorialspoint.com/java/index.htm). If you find these not useful, there are plenty of other sources you can find online. I’d suggest googling "Java for complete beginners" (NOTE:  If you search “Java for beginners” it often brings up tutorials for people who already understand programming and are just looking to learn Java.
) if you need other sources.

I’d use these to dip your feet into Java and then as a reference to tackle some programming problems. Specifically I would suggest trying out [Project Euler](https://projecteuler.net/) problems. They’re usually math related, but the first 20 or so problems are pretty easy to solve with code and help illustrate a lot of basic programming concepts. By doing these problems, you can really sharpen your programming skills on something achievable. If you have trouble solving them, you can always search up what a solution to the problem might look like, but you will want to make sure you actually understand how the solution works.

Beyond that, it's just about putting time and effort into learning Java, and doing different projects in it to learn more about the language. An easy way to learn even more is to try and automate boring, repetitive tasks you have to do. For example, if you have homework that involves plugging numbers into a few different formulas, you could write a program to take in the numbers and just compute the formula for you (NOTE:  This is something I actually had to do with math homework once. These annoying parts of your day can be eradicated with programming.
).

Another great trick is to look for projects that interest you and learn how to code for them, even if they seem above your current programming knowledge (NOTE:  For future senior programmers, this is an excellent way to teach new programmers. It's hard to get someone to just be inherently interested in programming, but if it’s connected to something they like, they’ll be much more into actually doing what you ask.). One programmer learned Java by doing code plugins for Spigot, a minecraft server application. I personally learned to program by trying to make machine learning models in Python (they didn’t work well, but that's not the point).

## Basics of Programming

Right now, you probably are thinking or screaming "What!? Garrett, you senile old fool, the basics of programming? Isn’t that just what the basics of Java are????" To which I have a few responses. First, if you are part of a team without me, how in the world did you learn my name. Second, its rude to call someone old and even worse to call someone senile. Beyond that, I’m only 18. But finally, no. The basics of programming and the basics of Java are related, but not the same.

The basics of Java involves simply learning the language. The basics of programming is actually applying it.

This is the section I expect to grow the most. As I, the original writer of the is documentation, write this, I’m not feeling too inspired, and there is certainly a lot of topics to cover in this area. It’s like the best practices, but for programming. As of the original writing, only one topic is covered, but I expect over time more and more topics will be covered.

### Algorithms

When it comes to programming, it can feel really enticing to just start writing the code with no direction and start adding in methods you think will be useful. But this is not a good strategy. You should get in the habit of writing out your code in steps before actually programming it. Thus, when you are programming it’s about converting your english into Java rather than trying to just write out the Java. This is a common bit of advice that is given to new programmers regardless of language.

You should write out your program like a recipe. It contains certain steps that, when followed, lead to a solution to your problem. Writing it out like this is called writing an algorithm. It’s useful to write it like this because your computer just takes each line of code and does what you tell it to do, so understanding what each step of the process looks like is a really good practice.

These steps should also be fairly specific. A computer does what you ask it to do. Nothing more. Nothing less. Computers are very powerful, but also very dumb. A computer is literally just a bunch of rocks we taught how to think. Once you realize that a computer is actually just a schizophrenic child with an autoimmune disorder, and that you shouldn’t beat it when it’s bad because it doesn’t know better, you will be a lot better off. We, as the programmers, have to do the thinking for it, and then the computer does the calculations for us. 

If you keep this all in mind, you will be able to write better programs faster.

## Robotics-Specific Programming

As you can imagine, coding a robot for an FRC competition is a little bit different than programming normally. Since it’s such a large undertaking, FRC provides a whole host of libraries and tools to make developing software to work with the electronic components a lot easier. A lot of coding for the robot is less about being good at Java, and more about being able to manipulate the tools given to us well. 

There are a few different tools we are given to make our programming easier. Here, they will be summarized, but more specific information is easier to glean off of the documentation made for these tools. Regardless, learning how to read technical documentation so that you can get something to work is an important skill. This is often called *grokking* (NOTE:  While it is a strange word, it isn’t completely made up. To grok is an actual English word.
)* *code bases.

I am more of a generalized programmer (NOTE:  And have been cursed to spend my days tending to the electronics. Because, as it stands, I’m one of the select few who knows how. In fact, after seeing how the other people who ‘know’ electronics handle putting them together, I am concerned I may be the only one who actually fully knows what’s going on.
), so the knowledge here will have to be expanded and elaborated on by other senior programmers, whether current or future (NOTE:  Furthermore, if any of the information in here is misleading or wrong, it should be corrected. Just like the rest of the documentation.
). But, I can describe some of the basics. (NOTE:  Note: these basics are meant to be read after getting a basic grasp of Java. I don’t want to bog down this discussion by taking time to describe things like libraries or methods
)

### WPILib

WPILib is the name given to a lot of the different software FRC releases to make programming easier. However, we mostly use WPILib to mean the libraries that FRC distributes. These give us access to a lot of different methods that allow us to quickly grab and put data onto the different hardware components. It makes it so that we don’t have to learn the ins and outs of every single hardware component and its software to program the robot. Essentially, it makes our lives significantly easier.

The WPILib installer (found [here](https://docs.wpilib.org/en/latest/docs/getting-started/getting-started-frc-control-system/wpilib-setup.html)) installs all the important applications you need onto your computer. It also comes with a special version of VS Code (NOTE:  Another application you can write code in. It’s a text editor, while IntelliJ, what we use, is an IDE. Why they are different things is not important, but IntelliJ comes with more nice features and is more useful to learn for real life coding than WPILib VS Code.
). Basically, if you need to use your laptop to drive the robot (NOTE:  Note: a section on deploying code to the robot and then how to drive the robot may be useful to add.
), the different board applications it installs (SmartDashboard and Shuffleboard) allow you to drive the robot. It also installs the WPILib libraries, but it’s only important for the Sam of any particular year to download this, because once the project is pushed to github, when other people pull the repo, they’ll just automatically install the libraries (NOTE:  Note: it may be important to add a section about setting up the programming project, so that knowledge is not lost.
). 

### The Scheduler

Overlooking all of the code we write (when written with WPILib and a Robot object) is a scheduler. While it is not important that you understand the ins and outs of how it exactly works, the scheduler basically takes all of the things you tell the robot to do and puts them into a queue. It will run tasks concurrently (NOTE:  Concurrently means "at the same time".) if it can and will run them in order if it can’t. Sometimes you need to directly put things into the scheduler, but that does not happen often.

### Commands and Subsystems

Our code is mainly structured into commands and subsystems. Commands describe actions that you would like a robot to take, whereas subsystems describe the actual parts of the robot, and some of the basic things they can do, in code form. While there are other forms of organization that are possible, this one, as of the 2020 season, seems like the most reasonable one to us senior programmers. It is easily understandable and makes the code nicely organized.

Commands usually will require that they have full reign over a subsystem, thus protecting us from making a drivetrain move both forwards and backwards at the same time because of conflicting commands.

Subsystems usually are written like a library of methods to pull from. You usually don’t actually put subsystems onto the scheduler, instead you call commands, which use the subsystem.

# Conclusion

That’s basically everything a starting programmer for this team needs to know. While this doesn’t cover every possible topic, that would be impossible to do, and this should leave you a little bit more privy to the brains of the robot. 

As I said at the start, it’s going to take a lot of work outside of reading one thing or making one program to become a programmer. It’s not impossible, it doesn’t even need to be hard, but it does require effort and motivation. Thus, you need to dare to be curious, step outside of your comfort zone and constantly be learning new things. This is only a primer and a start to your career as a programmer. Not only on this team, but maybe even to college and beyond.

I expect this document will be changing a lot. This should be an organic, growing and changing thing that reflects the ideas and knowledge of our team. It’s meant to preserve what we have learned and pass it onto future generations of programmers. To any senior programmers, it is up to you to be correcting, expanding and elaborating on the topics mentioned in this book with your own writing. Appendices, new chapters and new sections are all likely to happen.

Through this, I hope people can learn a thing or two, and that, one day, this is a fully sophisticated guide that can provide for all of our programmers (NOTE:  My name may be forgotten one day (which is Garrett Udstrand if you haven’t been paying attention, jeez, stop talking), but I hope this work lives on within the team. If I come back ten years from now, maybe we’ll still be using it).

# Appendix A - Wiring Schematic

The wiring schematic is hard to input directly into a Google Drive document. A GitHub repo of all of the schematic files can be found [here](https://github.com/Garrett45/FRC2225WiringSchema). The repo includes a pdf version of the schematic. However, it can also be found in the Google Drive folder that contains this documentation if you have access to that. And, I will link to the pdf right [here](https://drive.google.com/file/d/1isnbWtZhOwGeCKcv14U2U_1odNAlQmS6/view?usp=sharing).

The schematic shows off a very generalized wiring scheme. In this case, it would be the wiring of a robot with two Talon motor controllers. The actual schematic isn’t made to proper schematic standards, and simplifies a few of the components to make it easier to read. It is a logic-based schematic, meaning that it doesn’t try to preserve size or placement of the different components. Instead, it attempts to show the logic and wiring of the robot as cleanly as possible, even if it sacrifices proper spacing, sizing and placement.

The original schematic was created in KiCad (NOTE:  A free, open-source schematic making software that can be found here.
) and the GitHub repo contains the custom symbol library I made to put it together. Meaning that if someone were to install KiCad and pull the files from the GitHub repo, they would be able to modify my original work (NOTE:  This could be useful if a future Sam wishes to make the schematic properly to schematic standards. It could also be useful if an electronics group one year wanted to create a wiring schematic before wiring the robot together. THey could take the work I’ve already done and expand upon it. It would save time and effort.). 

[^1]: This is the contents of a footnote
